#include "eint.h"

#define BIT0 1


void EINTInit(void)
{

	vSemaphoreCreateBinary(xBinarySemaphoreModeSwitch);


	LPC_PINCON->PINSEL0    			&= ~(3<<0);	//P0.0 to GPIO function
	LPC_GPIO0->FIODIR      			&= ~BIT0;                 //set key pins to input (0)
	LPC_PINCON->PINMODE0  			=  3<<0; //pull down

	LPC_GPIOINT->IO0IntEnR  		|=  1;                 //enable rising edge INT (1)

	NVIC_EnableIRQ(EINT3_IRQn);                  //Enable EINT3 interrupt */

}


void EINT3_IRQHandler()
{

	NVIC_DisableIRQ(EINT3_IRQn);

	if (((LPC_GPIOINT->IO0IntStatR)&(1<<0)) == (1<<0))  {  //rising edge int?

	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;

	xSemaphoreGiveFromISR(xBinarySemaphoreModeSwitch,&xHigherPriorityTaskWoken);

	portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);

	LPC_GPIOINT->IO0IntClr = (1<<0);

	 }

	NVIC_EnableIRQ(EINT3_IRQn);



}
