################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../adc.c \
../circ_buffer.c \
../cr_startup_lpc17.c \
../eint.c \
../lpc17xx_clkpwr.c \
../lpc17xx_libcfg_default.c \
../lpc17xx_ssp.c \
../main.c \
../ssp.c 

OBJS += \
./adc.o \
./circ_buffer.o \
./cr_startup_lpc17.o \
./eint.o \
./lpc17xx_clkpwr.o \
./lpc17xx_libcfg_default.o \
./lpc17xx_ssp.o \
./main.o \
./ssp.o 

C_DEPS += \
./adc.d \
./circ_buffer.d \
./cr_startup_lpc17.d \
./eint.d \
./lpc17xx_clkpwr.d \
./lpc17xx_libcfg_default.d \
./lpc17xx_ssp.d \
./main.d \
./ssp.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -D__USE_CMSIS=CMSISv1p30_LPC17xx -D__CODE_RED -D__REDLIB__ -I"C:\Users\alex1104\Documents\LPCXpresso_4.1.5_219\workspace\FreeRTOS_Library\demo_code" -I"C:\Users\alex1104\Documents\LPCXpresso_4.1.5_219\workspace\CMSISv1p30_LPC17xx\inc" -I"C:\Users\alex1104\Documents\LPCXpresso_4.1.5_219\workspace\FreeRTOS_Library\include" -I"C:\Users\alex1104\Documents\LPCXpresso_4.1.5_219\workspace\FreeRTOS_Library\portable" -O0 -g3 -fsigned-char -c -fmessage-length=0 -fno-builtin -ffunction-sections -mcpu=cortex-m3 -mthumb -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


