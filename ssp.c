#include "ssp.h"


void SSPInit(void)
{
		volatile int timeKeeper=0;
		unsigned char i=0;
		SSP_CFG_Type sspChannelConfig;
		SSP_DATA_SETUP_Type sspDataConfig;

		uint8_t rxBuff[5];
		uint8_t txBuff[5];

		LPC_PINCON->PINSEL0 |= 0x2<<14; //SCK1
		LPC_PINCON->PINSEL0 |= 0x2<<18;	//MOSI1
//		PORT_CS->FIODIR 	|= 1<<2;	//P2.2 as CSn

		sspDataConfig.length = 1;
		sspDataConfig.tx_data = txBuff;
		sspDataConfig.rx_data = rxBuff;

		SSP_ConfigStructInit(&sspChannelConfig);
		SSP_Init(SSP_CHANNEL, &sspChannelConfig);
		SSP_Cmd(SSP_CHANNEL, ENABLE);
}
