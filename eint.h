#ifndef __EINT_H
#define __EINT_H

#include "LPC17xx.h"
#include "FreeRTOS.h"
#include "semphr.h"

#define EINT0 0
#define EINT1 1
#define EINT2 2
#define EINT3 3

#define EXTMODE0 0
#define EXTMODE1 1
#define EXTMODE2 2
#define EXTMODE3 3

#define EXTPOLAR0 0
#define EXTPOLAR1 1
#define EXTPOLAR2 2
#define EXTPOLAR3 3

xSemaphoreHandle xBinarySemaphoreModeSwitch;

void EINTInit(void);

#endif
