#ifndef __CIRC_BUFFER_H
#define __CIRC_BUFFER_H

#include "FreeRTOS.h"

#define CMAX 20

uint32_t circ[CMAX];
int pos;

void circ_update(uint32_t);
void circ_init();
uint32_t  circ_get();


#endif
