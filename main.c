/*
    FreeRTOS V6.1.1 - Copyright (C) 2011 Real Time Engineers Ltd.

    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation AND MODIFIED BY the FreeRTOS exception.
    ***NOTE*** The exception to the GPL is included to allow you to distribute
    a combined work that includes FreeRTOS without being obliged to provide the
    source code for proprietary components outside of the FreeRTOS kernel.
    FreeRTOS is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details. You should have received a copy of the GNU General Public 
    License and the FreeRTOS license exception along with FreeRTOS; if not it 
    can be viewed here: http://www.freertos.org/a00114.html and also obtained 
    by writing to Richard Barry, contact details for whom are available on the
    FreeRTOS WEB site.

    1 tab == 4 spaces!

    http://www.FreeRTOS.org - Documentation, latest information, license and
    contact details.

    http://www.SafeRTOS.com - A version that is certified for use in safety
    critical systems.

    http://www.OpenRTOS.com - Commercial support, development, porting,
    licensing and training services.
*/

/* FreeRTOS.org includes. */
#include "LPC17xx.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/* Demo includes. */
#include "basic_io.h"


#include "adc.h"
#include "ssp.h"
#include "eint.h"
#include "circ_buffer.h"

extern volatile uint32_t ADC0Value[ADC_NUM];
extern volatile uint32_t ADC0IntDone1;

extern xSemaphoreHandle xBinarySemaphoreModeSwitch;

#define  MODE_DISTANCE 	0
#define  MODE_VOLUME_CONTROL 	1
#define  MODE_BITE 2

#define  MAXIMUM		40
#define  MAXIMUM_OK		35

#define MINIMUM			20
#define MINIMUM_OK		25

int 	 iMode = MODE_DISTANCE;

extern uint32_t circ[CMAX];

/* The task function. */
void vTaskFunctionDisplayDistance( void *pvParameters );
void vTaskFunctionSwitchMode(void *pvParameters);
void vTaskFunctionGreenLED(void *pvParameters);
void vTaskFunctionRedLED(void *pvParameters);
void vTaskFunctionBITE(void *pvParameters);

xTaskHandle xTaskDisplayDistance;
xTaskHandle xTaskBITE;



/* Define the strings that will be passed in as the task parameters.  These are
defined const and off the stack to ensure they remain valid when the tasks are
executing. */
const char *pcTextForDisplayDistanceTask = "Display Distance\n";
const char *pcTextForModeSwitchTask = "Mode Switch\n";
const char *pcTextForModeGreenLed = "Green LED\n";
const char *pcTextForModeRedLed = "Red LED\n";
const char *pcTextForBITE = "BITE\n";


uint32_t distance = 30;



/*-----------------------------------------------------------*/

int main( void )
{

		//ADC
		ADCInit( ADC_CLK );

		EINTInit();

		//SSP
		SSPInit();

		//LEDS
		LEDInit();

		circ_init();

		iMode = MODE_BITE;


	/* Create the first task at priority 1... */
	xTaskCreate( vTaskFunctionDisplayDistance, "Display Distance", 240, (void*)pcTextForDisplayDistanceTask, 1, &xTaskDisplayDistance);
	xTaskCreate( vTaskFunctionSwitchMode, "Switch Mode", 240, (void*)pcTextForModeSwitchTask, 3, NULL );
	xTaskCreate( vTaskFunctionGreenLED,"Green Led",240,(void*)pcTextForModeGreenLed,2,NULL);
	xTaskCreate( vTaskFunctionRedLED,"Green Led",240,(void*)pcTextForModeRedLed,2,NULL);
	xTaskCreate( vTaskFunctionBITE, "BITE",240, (void*)pcTextForBITE,4,&xTaskBITE);


	/* Start the scheduler so our tasks start executing. */
	vTaskStartScheduler();	
	
	/* If all is well we will never reach here as the scheduler will now be
	running.  If we do reach here then it is likely that there was insufficient
	heap available for the idle task to be created. */
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

void vTaskFunctionBITE(void *pvParameters)
{
	int bStaticInit = 1;
	int bTestGreenLED = 0;
	int bTestRedLED = 0;
	int iCount = 0;

	portTickType xLastWakeTime;

	xLastWakeTime = xTaskGetTickCount();

	for (;bStaticInit != 0;)
	{
			SSP_SendData(SSP_CHANNEL,0x76);
			SSP_SendData(SSP_CHANNEL,84);
			SSP_SendData(SSP_CHANNEL,69);
			SSP_SendData(SSP_CHANNEL,83);
			SSP_SendData(SSP_CHANNEL,84);

			if (iCount == 1 || iCount == 3)
				LPC_GPIO0->FIOSET  = 1 << 4;
			else
				LPC_GPIO0->FIOCLR  = 1 << 4;

			if (iCount == 5 || iCount == 7)
				LPC_GPIO0->FIOSET  = 1 << 5;
			else
				LPC_GPIO0->FIOCLR  = 1 << 5;

			vTaskDelayUntil( &xLastWakeTime, 500 / portTICK_RATE_MS  );

			iCount+=1;

			if (iCount > 8)
			{
				iMode = MODE_DISTANCE;
				vTaskDelete(xTaskBITE);
			}
	}



}

void vTaskFunctionDisplayDistance( void *pvParameters )
{
	char *pcTaskName;
	portTickType xLastWakeTime;

	/* The string to print out is passed in via the parameter.  Cast this to a
	character pointer. */
	pcTaskName = ( char * ) pvParameters;

	/* The xLastWakeTime variable needs to be initialized with the current tick
	count.  Note that this is the only time we access this variable.  From this
	point on xLastWakeTime is managed automatically by the vTaskDelayUntil()
	API function. */
	xLastWakeTime = xTaskGetTickCount();

	/* As per most tasks, this task is implemented in an infinite loop. */
	for( ;; )
	{
		if (iMode == MODE_BITE)
			continue;

		int i = 0;
		for ( i = 0; i < 1; i++ )
		{
		  ADC0Read( i );
		  while ( !ADC0IntDone1 );
		  ADC0IntDone1 = 0;
		}



		uint32_t d = 512.0f / 4095.0f * 2.54f * ADC0Value[0];

		circ_update(d);

		uint32_t digits[4] = {0};

		distance = circ_average();

		digits[0] = (distance / 1000) % 10 + 48;
		digits[1]= (distance / 100) % 10 + 48;
		digits[2]= (distance / 10) % 10 + 48;
		digits[3]= (distance % 10) + 48;


		SSP_SendData(SSP_CHANNEL,0x76);
		SSP_SendData(SSP_CHANNEL,digits[0]);
		SSP_SendData(SSP_CHANNEL,digits[1]);
		SSP_SendData(SSP_CHANNEL,digits[2]);
		SSP_SendData(SSP_CHANNEL,digits[3]);

		vTaskDelayUntil( &xLastWakeTime, 50 / portTICK_RATE_MS  );

	}
}

void vTaskFunctionGreenLED(void *pvParameters)
{
	portTickType xLastWakeTime;

	xLastWakeTime = xTaskGetTickCount();

	int 	iFlashing = 0;


	for (;;)
	{
		if (iMode == MODE_BITE)
			continue;

		if (iMode == MODE_DISTANCE)
		{
			iFlashing = 0;
		}
		else
		{
			if (distance > MAXIMUM_OK)
				iFlashing = iFlashing == 0 ? 1 : 0;
			else if (distance > MINIMUM_OK)
				iFlashing = 1;
			else
				iFlashing = 0;
		}

		if (iFlashing == 1)
			LPC_GPIO0->FIOSET |= 1 << 4;
		else
			LPC_GPIO0->FIOCLR |= 1 << 4;


		vTaskDelayUntil( &xLastWakeTime, 500 / portTICK_RATE_MS  );
	}
}

void vTaskFunctionRedLED(void *pvParameters)
{

	portTickType xLastWakeTime;

	xLastWakeTime = xTaskGetTickCount();

	int 	iFlashing = 0;


		for (;;)
		{
			if (iMode == MODE_BITE)
				continue;

			if (iMode == MODE_DISTANCE)
			{
				iFlashing = 0;
			}
			else
			{
				if (distance > MINIMUM_OK)
					iFlashing = 0;
				else if (distance > MINIMUM)
					iFlashing = iFlashing == 0 ? 1 : 0;
				else
					iFlashing = 1;
			}

			if (iFlashing == 1)
				LPC_GPIO0->FIOSET |= 1 << 5;
			else
				LPC_GPIO0->FIOCLR |= 1 << 5;


			vTaskDelayUntil( &xLastWakeTime, 500 / portTICK_RATE_MS  );
		}
}



void vTaskFunctionSwitchMode(void *pvParameters)
{
	xSemaphoreTake(xBinarySemaphoreModeSwitch,0);

	for( ;; )
	{
		xSemaphoreTake(xBinarySemaphoreModeSwitch,portMAX_DELAY);
		//vPrintString("Switch Mode semaphore is taken\n");

		switch (iMode)
		{
		case	MODE_DISTANCE:
			iMode = MODE_VOLUME_CONTROL;

			break;
		case 	MODE_VOLUME_CONTROL:
			iMode = MODE_DISTANCE;

			break;
		}




	}

}

void LEDInit()
{
		LPC_PINCON->PINSEL0    			&= ~(3<<8);	//P0.5 to GPIO function
		LPC_GPIO0->FIODIR      			|= 1 << 4;                 //set key pins to output
		LPC_GPIO0->FIOSET 				|= 1 << 4;

		LPC_PINCON->PINSEL0    			&= ~(3<<10);	//P0.5 to GPIO function
		LPC_GPIO0->FIODIR      			|= 1 << 5;                 //set key pins to output
		LPC_GPIO0->FIOSET				|= 1 << 5;

}
/*-----------------------------------------------------------*/

void vApplicationMallocFailedHook( void )
{
	/* This function will only be called if an API call to create a task, queue
	or semaphore fails because there is too little heap RAM remaining. */
	for( ;; );
}
/*-----------------------------------------------------------*/

void vApplicationStackOverflowHook( xTaskHandle *pxTask, signed char *pcTaskName )
{
	/* This function will only be called if a task overflows its stack.  Note
	that stack overflow checking does slow down the context switch
	implementation. */
	for( ;; );
}
/*-----------------------------------------------------------*/

void vApplicationIdleHook( void )
{
	/* This example does not use the idle hook to perform any processing. */
}
/*-----------------------------------------------------------*/

void vApplicationTickHook( void )
{
	/* This example does not use the tick hook to perform any processing. */
}




