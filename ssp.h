#ifndef __SSP_H
#define __SSP_H

#include "lpc17xx_ssp.h"

#define SSP_CHANNEL LPC_SSP1

void SSPInit(void);

#endif
