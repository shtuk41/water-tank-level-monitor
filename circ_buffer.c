
#include "circ_buffer.h"


void circ_update(uint32_t xnew)
{
	pos = ((pos == CMAX - 1) ? 0 : (pos + 1));
	circ[pos] = xnew;
}

void circ_init()
{
	int i;

	for (i = 0; i < CMAX; i++)
		circ[i] = 0;

	pos = CMAX - 1;
}

uint32_t  circ_get(int i)
{
	int ii;

	ii = (pos - i) % CMAX;

	return circ[ii];
}

uint32_t circ_average()
{
	int i;

	uint32_t total = 0;

	for (i = 0; i < CMAX; i++)
		total += circ[i];

	total /= CMAX;

	return total;

}
